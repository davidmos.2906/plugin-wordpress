<?php 
/**
 * Plugin Name: Prueba tecnica Luis David Montes Mosquera
 * Description: Desarrollar un plugin para Wordpress que muestre un formulario dependiente de países y ciudades
 * Version: 1.0.0
 * Author: Luis David Montes
 */

add_shortcode( "prueba_tecnica", function($atts, $content){
	$output = '
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
            Abrir
        </button>
        <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Prueba Opción 1</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Desarrollar un plugin para Wordpress que muestre un formulario dependiente de países y ciudades.
                            Descripción: El plugin debe cumplir los siguientes requisitos:
                        </p>
                        <ul>
                            <li>Usar el api de http://www.geonames.org/ (o cualquier otro) para la lista de países y ciudades.</li>
                            <li>Ser responsive</li>
                            <li>Usar Bootstrap</li>
                        </ul>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Pais</label>
                                <select class="form-select" name="select_pais" id="select_pais" onchange="consultarDepartamento(this.value)">
                                    <option selected disabled> - Seleccione - </option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Departamento</label>
                                <select class="form-select" name="select_departamento" id="select_departamento">
                                    <option selected disabled> - Seleccione - </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        
	return $output;
});

add_action('wp_footer', function(){?>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>

        var myModal = new bootstrap.Modal(document.getElementById('myModal'), {backdrop : true});
        myModal.show();

        fetch('http://api.geonames.org/countryInfoJSON?&username=david1')
        .then(response => response.json())
        .then(data => getPais(data));

        function getPais(data){

            const select_pais = document.querySelector('#select_pais');

            for(let i = 0 ; i < data.geonames.length; i ++){
                const option = document.createElement('option');
                option.value = data.geonames[i].geonameId;
                option.text = data.geonames[i].countryName;
                select_pais.appendChild(option);            
            }

        }

        function consultarDepartamento(geonameId){
            
            fetch('http://api.geonames.org/childrenJSON?geonameId='+geonameId+'&username=david1')
            .then(response => response.json())
            .then(data => getDepartamento(data));
        }

        function getDepartamento(data){

            const select_departamento = document.querySelector('#select_departamento');

            for (let i = select_departamento.options.length; i >= 0; i--) {
                select_departamento.remove(i);
            }   

            for(let i = 0 ; i < data.geonames.length; i ++){
                const option = document.createElement('option');
                option.value = data.geonames[i].geonameId;
                option.text = data.geonames[i].name;
                select_departamento.appendChild(option);            
            }
        }

    </script>
    <?php }  ,1); ?>